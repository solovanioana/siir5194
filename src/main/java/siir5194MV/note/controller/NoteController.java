package siir5194MV.note.controller;

import java.util.HashMap;
import java.util.List;

import siir5194MV.note.model.Corigent;
import siir5194MV.note.model.Elev;
import siir5194MV.note.model.Medie;
import siir5194MV.note.model.Nota;
import siir5194MV.note.repository.ClasaRepository;
import siir5194MV.note.repository.ClasaRepositoryMock;
import siir5194MV.note.repository.EleviRepository;
import siir5194MV.note.repository.EleviRepositoryMock;
import siir5194MV.note.repository.NoteRepository;
import siir5194MV.note.repository.NoteRepositoryMock;
import siir5194MV.note.utils.ClasaException;

public class NoteController {
	private NoteRepository note;
	private ClasaRepository clasa;
	private EleviRepository elevi;

	public NoteController() {
		note = new NoteRepositoryMock();
		clasa = new ClasaRepositoryMock();
		elevi = new EleviRepositoryMock();
	}
	
	public void addNota(Nota nota) throws ClasaException {
		note.addNota(nota);
	}
	
	public void addElev(Elev elev) {
		elevi.addElev(elev);
	}
	
	public void creeazaClasa(List<Elev> elevi, List<Nota> note) {
		clasa.creazaClasa(elevi, note);
	}
	
	public List<Medie> calculeazaMedii() throws ClasaException {
			return clasa.calculeazaMedii();
		
	}
	
	public List<Nota> getNote() {
		return note.getNote();
	}
	
	public List<Elev> getElevi() {
		return elevi.getElevi();
	}
	
	public HashMap<Elev, HashMap<String, List<Integer>>> getClasa() {
		return clasa.getClasa();
	}
	
	public void afiseazaClasa() {
		clasa.afiseazaClasa();
	}
	
	public void readElevi(String fisier) {
		elevi.readElevi(fisier);
	}
	
	public void readNote(String fisier) {
		note.readNote(fisier);
	}
	
	public List<Corigent> getCorigenti() {
		return clasa.getCorigenti();
	}
}

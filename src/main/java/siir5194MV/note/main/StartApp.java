package siir5194MV.note.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import siir5194MV.note.model.Nota;
import siir5194MV.note.utils.ClasaException;

import siir5194MV.note.model.Corigent;
import siir5194MV.note.model.Medie;

import siir5194MV.note.controller.NoteController;

//functionalitati
//F01.	 adaugarea unei note la o anumita materie (nr. matricol, materie, nota acordata);
//F02.	 calcularea mediilor semestriale pentru fiecare elev (nume, nr. matricol),
//F03.	 afisarea elevilor coringenti, ordonati descrescator dupa numarul de materii la care nu au promovat şi alfabetic dupa nume.


public class StartApp {

	/**
	 * @param args
	 * @throws ClasaException
	 */
	public static void main(String[] args) throws ClasaException {
		// TODO Auto-generated method stub
		NoteController noteController = new NoteController();
		List<Medie> medii = new LinkedList<Medie>();
		List<Corigent> corigenti = new ArrayList<Corigent>();
		noteController.readElevi(args[0]);
		noteController.readNote(args[1]);
		noteController.creeazaClasa(noteController.getElevi(), noteController.getNote());
		boolean toExit = false;
		while(!toExit) {
			System.out.println("1. Adaugare Nota");
			System.out.println("2. Calculeaza medii");
			System.out.println("3. Elevi corigenti");
			System.out.println("4. Iesire");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in) );
		    try {
				int option = Integer.parseInt(br.readLine());
				switch(option) {
				case 1:
					System.out.println("Id student:");
					int id = Integer.parseInt(br.readLine());
					System.out.println("Materie:");
					String materie = br.readLine();
					System.out.println("Nota:");
					int nota = Integer.parseInt(br.readLine());
					Nota nota1 = new Nota(id, materie, nota);
					noteController.addNota(nota1);
					System.out.println("Nota adaugata");
						break;
				case 2: medii = noteController.calculeazaMedii();
						for(Medie medie:medii)
							System.out.println(medie);
						break;
				case 3: corigenti = noteController.getCorigenti();
						for(Corigent corigent:corigenti)
							System.out.println(corigent);
						break;
				case 4: toExit = true;
						break;
				default: System.out.println("Introduceti o optiune valida!");
				}
				
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}

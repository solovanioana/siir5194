package siir5194MV.note.repository;

import java.util.List;

import siir5194MV.note.utils.ClasaException;

import siir5194MV.note.model.Nota;

public interface NoteRepository {
	
	public void addNota(Nota nota) throws ClasaException;
	public List<Nota> getNote(); 
	public void readNote(String fisier);
	
}

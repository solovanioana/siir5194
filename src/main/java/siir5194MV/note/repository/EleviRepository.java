package siir5194MV.note.repository;

import java.util.List;

import siir5194MV.note.model.Elev;

public interface EleviRepository {
	public void addElev(Elev e);
	public List<Elev> getElevi();
	public void readElevi(String fisier);
}

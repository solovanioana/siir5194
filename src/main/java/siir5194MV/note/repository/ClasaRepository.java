package siir5194MV.note.repository;

import siir5194MV.note.model.Corigent;
import siir5194MV.note.model.Elev;
import siir5194MV.note.model.Medie;
import siir5194MV.note.model.Nota;
import siir5194MV.note.utils.ClasaException;

import java.util.HashMap;
import java.util.List;

public interface ClasaRepository {
	
	public void creazaClasa(List<Elev> elevi, List<Nota> note);
	public HashMap<Elev, HashMap<String, List<Integer>>> getClasa();
	public List<Medie> calculeazaMedii() throws ClasaException;
	public void afiseazaClasa();
	public List<Corigent> getCorigenti();
}

package siir5194MV.note.repository;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import siir5194MV.note.controller.NoteController;
import siir5194MV.note.model.Corigent;
import siir5194MV.note.model.Elev;
import siir5194MV.note.model.Nota;
import siir5194MV.note.utils.ClasaException;
import siir5194MV.note.utils.Constants;

import java.util.List;

import static org.junit.Assert.*;

public class GetCorigentiTest {
    private NoteController ctrl;

    @Before
    public void setUp() throws Exception {
        ctrl = new NoteController();
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testValidGetCorigenti() throws ClasaException {
        Elev e1 = new Elev(1, "Elev1");
        Elev e2 = new Elev(2, "Elev2");
        ctrl.addElev(e1);
        ctrl.addElev(e2);
        Nota nota1 = new Nota(1,"Romana",4);
        Nota n1 = new Nota(1,"Romana",4);
        Nota nota2 = new Nota(2,"Engleza",5);
        Nota n2 = new Nota(2,"Engleza", 3);
        ctrl.addNota(nota1);
        ctrl.addNota(n1);
        ctrl.addNota(nota2);
        ctrl.addNota(n2);
        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        List<Corigent> corigenti1 = ctrl.getCorigenti();
        assertEquals(corigenti1.get(0).getNrMaterii(),corigenti1.get(1).getNrMaterii());
    }

    @Test
    public void testInvalidGetCorigenti() throws ClasaException {
        expectedEx.expectMessage(Constants.emptyRepository);
        List<Corigent> corigents = ctrl.getCorigenti();
    }
}
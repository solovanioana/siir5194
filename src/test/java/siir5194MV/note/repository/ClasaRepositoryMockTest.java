package siir5194MV.note.repository;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import siir5194MV.note.controller.NoteController;
import siir5194MV.note.model.Elev;
import siir5194MV.note.model.Medie;
import siir5194MV.note.model.Nota;
import siir5194MV.note.utils.ClasaException;
import siir5194MV.note.utils.Constants;

import java.util.List;

import static org.junit.Assert.*;

public class ClasaRepositoryMockTest {
    private NoteController ctrl;

    @Before
    public void init(){
        ctrl = new NoteController();
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void test1() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.emptyRepository);
        ctrl.calculeazaMedii();
    }

    @Test
    public void test2() throws ClasaException {
        Elev e1 = new Elev(1, "Andrei");
        ctrl.addElev(e1);
        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        List<Medie> rezultate = ctrl.calculeazaMedii();
        assertEquals(rezultate.size(),1);
        for(Medie m : rezultate)
            if(m.getElev().getNrmatricol() == 1)
                assertEquals(m.getMedie(),0,0.001);

    }



    @Test
    public void test3() throws ClasaException {
        Elev e1 = new Elev(1, "Andrei");
        ctrl.addElev(e1);
        Nota n1 = new Nota(1,"Matematica", 10);
        ctrl.addNota(n1);
        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        List<Medie> rezultate = ctrl.calculeazaMedii();
        for(Medie m : rezultate)
            if(m.getElev().getNrmatricol() == 1)
                assertEquals(m.getMedie(),10,0.001);
    }

    @Test
    public void test4() throws ClasaException {
        Elev e1 = new Elev(1, "Andrei");
        Elev e2 = new Elev(2, "Alexia");
        ctrl.addElev(e1);
        ctrl.addElev(e2);
        Nota n1 = new Nota(1,"Matematica", 10);
        Nota n2 = new Nota(1, "Matematica", 8);
        Nota n3 = new Nota(2, "Matematica", 10);
        ctrl.addNota(n1);
        ctrl.addNota(n2);
        ctrl.addNota(n3);
        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        List<Medie> rezultate = ctrl.calculeazaMedii();
        for(Medie m : rezultate)
        {
            if(m.getElev().getNrmatricol() == 1)
            {
                assertEquals(m.getMedie(),9,0.001);
            }
            if(m.getElev().getNrmatricol() == 2)
            {
                assertEquals(m.getMedie(), 10, 0.001);
            }
        }

    }
}